<?php

namespace ZohoInvoice\Adapter\Http;

use GuzzleHttp\Client;
use Monolog\Logger;
use ZohoInvoice\Adapter\InvoiceAdapterInterface;

/**
 * Class HttpZohoAdapter.
 *
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
abstract class AbstractHttpZohoAdapter implements InvoiceAdapterInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var array
     */
    protected $config;

    /**
     * HttpZohoAdapter constructor.
     * @param Logger $logger
     * @param Client $client
     * @param array $config
     */
    public function __construct(Logger $logger, Client $client, array $config)
    {
        $this->logger = $logger;
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @param $message
     */
    protected function log($message)
    {
        $this->logger->addError(sprintf('%s - %s', 'ZOHO', $message));
    }
}
