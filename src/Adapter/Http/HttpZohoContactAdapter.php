<?php

namespace ZohoInvoice\Adapter\Http;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use ZohoInvoice\Adapter\ContactAdapterInterface;
use ZohoInvoice\Builder\ContactBuilder;
use ZohoInvoice\Invoice\InvoiceException;
use ZohoInvoice\Invoice\ZohoContact\ZohoContact;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;
use ZohoInvoice\Invoice\ZohoTranslator\ZohoContactTranslator;

/**
 * Class HttpZohoContactAdapter.
 *
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class HttpZohoContactAdapter extends AbstractHttpZohoAdapter implements ContactAdapterInterface
{
    /**
     * @param ZohoId $zohoId
     * @return \ZohoInvoice\Invoice\ZohoResponse\ZohoSingleContactResponse
     * @throws InvoiceException
     */
    public function getContact(ZohoId $zohoId)
    {
        try {
            $uri = sprintf(
                'contacts/%s?authtoken=%s',
                $zohoId->toString(),
                $this->config['authtoken']
            );
            $response = $this->client->request('GET', $uri);

            if (200 == $response->getStatusCode()) {
                $body = $response->getBody();

                return (new ZohoContactTranslator())->convertToSingleResponse($body->getContents());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho get contact bad request');
        } catch (RequestException $e) {
            $this->log(Psr7\str($e->getRequest()));

            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @return \ZohoInvoice\Invoice\ZohoResponse\ZohoCollectionContactResponse
     * @throws InvoiceException
     */
    public function getContacts()
    {
        try {
            $response = $this->client->request('GET', 'contacts', [
                'query' => [
                    'authtoken' => $this->config['authtoken'],
                ],
            ]);

            if (200 == $response->getStatusCode()) {
                $body = $response->getBody();

                return (new ZohoContactTranslator())->convertToCollectionResponse($body->getContents());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho get contact bad request');
        } catch (RequestException $e) {
            $this->log(Psr7\str($e->getRequest()));

            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoContact $zohoContact
     * @return \ZohoInvoice\Invoice\ZohoResponse\ZohoSingleContactResponse
     * @throws InvoiceException
     */
    public function createContact(ZohoContact $zohoContact)
    {
        try {
            $response = $this->client->request('POST', 'contacts', [
                'query' => [
                    'authtoken' => $this->config['authtoken'],
                    'JSONString' => json_encode(ContactBuilder::buildArray($zohoContact)),
                ],
            ]);

            if (201 == $response->getStatusCode()) {
                $body = $response->getBody();

                return (new ZohoContactTranslator())->convertToSingleResponse($body->getContents());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho create contact bad request');
        } catch (RequestException $e) {
            $this->log(Psr7\str($e->getRequest()));

            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoContact $zohoContact
     * @return \ZohoInvoice\Invoice\ZohoResponse\ZohoSingleContactResponse
     * @throws InvoiceException
     */
    public function updateContact(ZohoContact $zohoContact)
    {
        try {
            $response = $this->client->request('PUT', 'contacts/'.$zohoContact->zohoId()->toString(), [
                'query' => [
                    'authtoken' => $this->config['authtoken'],
                    'JSONString' => json_encode(ContactBuilder::buildArray($zohoContact)),
                ],
            ]);

            if (200 == $response->getStatusCode()) {
                $body = $response->getBody();

                return (new ZohoContactTranslator())->convertToSingleResponse($body->getContents());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho update contact bad request');
        } catch (RequestException $e) {
            $this->log(Psr7\str($e->getRequest()));

            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoContact $zohoContact
     * @return \ZohoInvoice\Invoice\ZohoResponse\ZohoSingleContactResponse
     * @throws InvoiceException
     */
    public function createLightContact($email)
    {
        try {
            $response = $this->client->request('POST', 'contacts', [
                'query' => [
                    'authtoken' => $this->config['authtoken'],
                    'JSONString' => json_encode([
                        'contact_name' => $email,
                        'contact_persons' => [
                            [
                                'email' => $email,
                                'is_primary_contact' => true,
                            ],
                        ],
                    ]),
                ],
            ]);

            if (201 == $response->getStatusCode()) {
                $body = $response->getBody();

                return (new ZohoContactTranslator())->convertToSingleResponse($body->getContents());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho create light contact bad request');
        } catch (RequestException $e) {
            $this->log(Psr7\str($e->getRequest()));

            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            throw new InvoiceException('Zoho exception');
        }
    }
}
