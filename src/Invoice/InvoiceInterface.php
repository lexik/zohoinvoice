<?php

namespace ZohoInvoice\Invoice;

use ZohoInvoice\Adapter\ContactAdapterInterface;
use ZohoInvoice\Adapter\InvoiceEstimateAdapterInterface;
use ZohoInvoice\Adapter\InvoiceInvoiceAdapterInterface;

interface InvoiceInterface
{
    /**
     * @return ContactAdapterInterface
     */
    public function getContactAdapter();

    /**
     * @return InvoiceEstimateAdapterInterface
     */
    public function getEstimateAdapter();

    /**
     * @return InvoiceInvoiceAdapterInterface
     */
    public function getInvoiceAdapter();
}
