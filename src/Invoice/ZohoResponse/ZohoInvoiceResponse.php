<?php

namespace ZohoInvoice\Invoice\ZohoResponse;


use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoiceId;

class ZohoInvoiceResponse extends ZohoResponse
{
    /**
     * @var ZohoInvoiceId
     */
    public $invoiceId;

    public static function create($invoiceId, $exist = true)
    {
        $response = new self();
        $response->invoiceId = ZohoInvoiceId::createFromString($invoiceId);
        $response->exist = $exist;

        return $response;
    }
}
