<?php

namespace ZohoInvoice\Invoice\ZohoResponse;

use ZohoInvoice\Invoice\ZohoContact\ZohoContact;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;

class ZohoCollectionContactResponse extends ZohoResponse
{
    /**
     * @var array|ZohoContact[]
     */
    public $contacts;

    public static function createMany(array $contacts)
    {
        $response = new self();

        $response->contacts = array_map(function ($contact) {
            $contactId = $contact['contact_id'];
            unset($contact['contact_id']);

            return ZohoContact::create(
                ZohoId::createFromString($contactId),
                $contact
            );
        }, $contacts);

        return $response;
    }
}