<?php

namespace ZohoInvoice\Builder;

use ZohoInvoice\Invoice\ZohoContact\ZohoContact;

class ContactBuilder
{
    public static function buildArray(ZohoContact $zohoContact)
    {
        return [
            'contact_name' => $zohoContact->param('contact_name'),
            'company_name' => $zohoContact->param('company_name'),
            'contact_persons' => [
                [
                    'email' => $zohoContact->param('email'),
                    'first_name' => $zohoContact->param('first_name'),
                    'last_name' => $zohoContact->param('last_name'),
                    'is_primary_contact' => true,
                ],
            ],
            'billing_address' => $zohoContact->param('billing_address'),
        ];
    }
}