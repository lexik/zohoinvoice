<?php

namespace ZohoInvoice\Adapter;

interface InvoiceEstimateAdapterInterface extends InvoiceAdapterInterface
{
    public function createQuote();

    public function updateQuote();

    public function sendQuoteToEmail();

    public function downloadQuote();

    public function getQuote();
}
