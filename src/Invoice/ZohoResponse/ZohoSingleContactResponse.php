<?php

namespace ZohoInvoice\Invoice\ZohoResponse;

use ZohoInvoice\Invoice\ZohoContact\ZohoContact;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;

class ZohoSingleContactResponse extends ZohoResponse
{
    /**
     * @var ZohoContact
     */
    public $contact;

    public static function createSingle($contact)
    {
        $response = new self();

        $contactId = $contact['contact_id'];
        unset($contact['contact_id']);

        $response->contact = ZohoContact::create(
            ZohoId::createFromString($contactId),
            $contact
        );

        return $response;
    }
}
