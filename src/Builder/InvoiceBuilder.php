<?php

namespace ZohoInvoice\Builder;

use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoice;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoPayment;

class InvoiceBuilder
{
    public static function createInvoiceArray(ZohoInvoice $zohoInvoice)
    {
        $lines = [];

        foreach ($zohoInvoice->items() as $invoiceItem) {
            $lines[] = [
                'item_id' => $invoiceItem->itemId(),
                'name' => $invoiceItem->name(),
                'rate' => $invoiceItem->rate(),
                'quantity' => $invoiceItem->quantity(),
            ];
        }

        return [
            'customer_id' => $zohoInvoice->zohoContactId()->toString(),
            'is_inclusive_tax' => false,
            'line_items' => $lines,
        ];
    }

    public static function createSinglePaymentArray(ZohoPayment $zohoPayment)
    {
        return [
                'customer_id' => $zohoPayment->zohoId()->toString(),
                'payment_mode' => $zohoPayment->paymentMode(),
                'amount' => $zohoPayment->amount(),
                'date' => $zohoPayment->date(),
                'description' => $zohoPayment->description(),
                'invoices' => [
                    'invoice_id' => $zohoPayment->singleInvoice()->zohoInvoiceId()->toString(),
                    'amount_applied' => $zohoPayment->singleInvoice()->amount(),
                ],
            ];
    }
}