<?php

namespace ZohoInvoice\Invoice\ZohoResponse;

use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoice;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoiceId;

class ZohoCollectionInvoiceResponse extends ZohoResponse
{
    public $invoices;

    public static function createMany(array $invoices)
    {
        $response = new self();

        $response->invoices = array_map(function ($invoice) {
            $invoiceId = $invoice['invoice_id'];

            unset($invoice['invoice_id']);
            return ZohoInvoice::create(
                ZohoInvoiceId::createFromString($invoiceId),
                $invoice
            );
        }, $invoices);

        return $response;
    }
}