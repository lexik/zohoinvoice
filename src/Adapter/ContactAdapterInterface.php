<?php

namespace ZohoInvoice\Adapter;

use ZohoInvoice\Invoice\InvoiceException;
use ZohoInvoice\Invoice\ZohoContact\ZohoContact;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;
use ZohoInvoice\Invoice\ZohoResponse\ZohoSingleContactResponse;

interface ContactAdapterInterface extends InvoiceAdapterInterface
{
    /**
     * @param ZohoId $zohoId
     * @return mixed
     */
    public function getContact(ZohoId $zohoId);

    /**
     * @return \ZohoInvoice\Invoice\ZohoResponse\ZohoCollectionContactResponse
     * @throws InvoiceException
     */
    public function getContacts();

    /**
     * @param ZohoContact $zohoContact
     *
     * @throws \Exception
     *
     * @return ZohoSingleContactResponse
     */
    public function createContact(ZohoContact $zohoContact);

    /**
     * @param ZohoContact $zohoContact
     *
     * @throws \Exception
     *
     * @return ZohoSingleContactResponse
     */
    public function updateContact(ZohoContact $zohoContact);

    /**
     * @param $email
     * @return mixed
     */
    public function createLightContact($email);
}
