<?php

namespace ZohoInvoice\Invoice;
use ZohoInvoice\Adapter\ContactAdapterInterface;
use ZohoInvoice\Adapter\InvoiceEstimateAdapterInterface;
use ZohoInvoice\Adapter\InvoiceInvoiceAdapterInterface;

/**
 * Class InvoiceService.
 *
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
final class InvoiceService implements InvoiceInterface
{
    /**
     * @var InvoiceEstimateAdapterInterface
     */
    private $invoiceEstimateAdapter;

    /**
     * @var ContactAdapterInterface
     */
    private $invoiceContactAdapter;

    /**
     * @var InvoiceInvoiceAdapterInterface
     */
    private $invoiceAdapter;

    /**
     * InvoiceService constructor.
     *
     * @param InvoiceEstimateAdapterInterface $invoiceEstimateAdapter
     * @param ContactAdapterInterface  $invoiceContactAdapter
     * @param InvoiceInvoiceAdapterInterface  $invoiceAdapter
     */
    public function __construct(
        InvoiceEstimateAdapterInterface $invoiceEstimateAdapter, ContactAdapterInterface $invoiceContactAdapter,
        InvoiceInvoiceAdapterInterface $invoiceAdapter)
    {
        $this->invoiceEstimateAdapter = $invoiceEstimateAdapter;
        $this->invoiceContactAdapter = $invoiceContactAdapter;
        $this->invoiceAdapter = $invoiceAdapter;
    }

    /**
     * @return ContactAdapterInterface
     */
    public function getContactAdapter()
    {
        return $this->invoiceContactAdapter;
    }

    /**
     * @return InvoiceEstimateAdapterInterface
     */
    public function getEstimateAdapter()
    {
        return $this->invoiceEstimateAdapter;
    }

    /**
     * @return InvoiceInvoiceAdapterInterface
     */
    public function getInvoiceAdapter()
    {
        return $this->invoiceAdapter;
    }
}
