<?php

namespace ZohoInvoice\Invoice\ZohoContact;

/**
 * Class ZohoContact
 * @package ZohoInvoice\ZohoContact
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class ZohoContact
{
    /**
     * @var ZohoId
     */
    private $zohoId;

    private $params;

    /**
     * ZohoContact constructor.
     * @param ZohoId $zohoId
     * @param $params
     */
    public function __construct(ZohoId $zohoId, $params)
    {
        $this->zohoId = $zohoId;
        $this->params = $params;
    }

    public static function create(ZohoId $zohoId, array $params)
    {
        return new self($zohoId, $params);
    }

    public function zohoId()
    {
        return $this->zohoId;
    }

    /**
     * @return mixed
     */
    public function params()
    {
        return $this->params;
    }

    public function param($key)
    {
        if (isset($this->params[$key])) {
            return $this->params[$key];
        }

        return null;
    }
}