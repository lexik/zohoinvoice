<?php

namespace ZohoInvoice\Adapter\Http;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use ZohoInvoice\Adapter\InvoiceInvoiceAdapterInterface;
use ZohoInvoice\Builder\InvoiceBuilder;
use ZohoInvoice\Invoice\InvoiceException;
use ZohoInvoice\Invoice\ZohoContact\ZohoContact;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoice;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoiceId;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoPayment;
use ZohoInvoice\Invoice\ZohoResponse\ZohoResponse;
use ZohoInvoice\Invoice\ZohoResponse\ZohoInvoiceResponse;
use ZohoInvoice\Invoice\ZohoTranslator\ZohoInvoiceTranslator;

/**
 * Class HttpZohoInvoiceAdapter.
 *
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class HttpZohoInvoiceAdapter extends AbstractHttpZohoAdapter implements InvoiceInvoiceAdapterInterface
{
    public function getInvoice(ZohoInvoiceId $zohoInvoiceId)
    {
        try {
            $uri = sprintf('invoices/%s?authtoken=%s', $zohoInvoiceId->toString(), $this->config['authtoken']);
            $response = $this->client->request('GET', $uri);

            if (200 == $response->getStatusCode()) {
                return ZohoInvoiceResponse::create($zohoInvoiceId->toString());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho get invoice no response');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                if (404 == $e->getResponse()->getStatusCode()) {
                    $response = new ZohoResponse();
                    $response->exist = false;

                    return $response;
                } else {
                    $this->log(Psr7\str($e->getResponse()));
                }
            }

            $this->log(Psr7\str($e->getRequest()));

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoInvoice $zohoInvoice
     *
     * @throws InvoiceException
     *
     * @return mixed|ZohoInvoiceResponse|ZohoResponse|\Psr\Http\Message\ResponseInterface
     */
    public function createInvoice(ZohoInvoice $zohoInvoice)
    {
        try {
            $uri = sprintf(
                'invoices/?authtoken=%s&send=false&ignore_auto_number_generation=false&JSONString=%s',
                $this->config['authtoken'],
                json_encode(InvoiceBuilder::createInvoiceArray($zohoInvoice), JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE)
            );

            $response = $this->client->request('POST', $uri);

            if (201 == $response->getStatusCode()) {
                $body = $response->getBody();

                return ZohoInvoiceTranslator::convertToResponse($body);
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho create invoice');
        } catch (ClientException $e) {
            if ($e->hasResponse()) {
                if (404 == $e->getResponse()->getStatusCode()) {
                    $response = new ZohoResponse();
                    $response->exist = false;

                    return $response;
                } else {
                    $this->log(Psr7\str($e->getResponse()));
                }
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }
            $this->log(Psr7\str($e->getRequest()));
            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoPayment $zohoPayment
     * @return mixed|\Psr\Http\Message\ResponseInterface|ZohoInvoiceResponse
     * @throws InvoiceException
     */
    public function createPaymentInvoice(ZohoPayment $zohoPayment)
    {
        try {
            $uri = sprintf(
                'customerpayments/?authtoken=%s&JSONString=%s',
                $this->config['authtoken'],
                json_encode(InvoiceBuilder::createSinglePaymentArray($zohoPayment), JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE)
            );

            $response = $this->client->request('POST', $uri);

            if (201 == $response->getStatusCode()) {
                $firstInvoice = $zohoPayment->paymentInvoices()[0];
                $response = ZohoInvoiceResponse::create($firstInvoice);

                return $response;
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho create payment invoice');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            $this->log(Psr7\str($e->getRequest()));

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoPayment $zohoPayment
     *
     * @throws InvoiceException
     *
     * @return ZohoResponse
     */
    public function emailAnInvoice(ZohoPayment $zohoPayment, ZohoContact $zohoContact)
    {
        try {
            $uri = sprintf(
                'invoices/%s/email?authtoken=%s&JSONString=%s',
                $zohoPayment->singleInvoice()->zohoInvoiceId()->toString(),
                $this->config['authtoken'],
                json_encode(
                    [
                        'send_from_org_email_id' => false,
                        'to_mail_ids' => [
                            $this->config['test_mode'] ? $this->config['delivery_emails'] : $zohoContact->email(),
                        ],
                    ], JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE)
            );

            $response = $this->client->request('POST', $uri);

            if (200 == $response->getStatusCode()) {
                return new ZohoResponse();
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho send email invoice bad request');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            $this->log(Psr7\str($e->getRequest()));

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoInvoiceId $invoicesIds
     * @return string
     * @throws InvoiceException
     */
    public function downloadInvoice($invoicesIds)
    {
        try {
            $response = $this->client->request('GET', 'invoices/pdf', [
                'query' => [
                    'authtoken' => $this->config['authtoken'],
                    'invoice_ids' => $invoicesIds,
                ],
            ]);

            if (200 == $response->getStatusCode()) {
                $body = $response->getBody();

                return $body->getContents();
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho download invoice pdf');
        } catch (RequestException $e) {
            $this->log(Psr7\str($e->getRequest()));

            if ($e->hasResponse()) {
                $this->log(Psr7\str($e->getResponse()));
            }

            throw new InvoiceException('Zoho exception');
        }
    }

    /**
     * @param ZohoId $zohoId
     * @param $status
     * @return mixed|\Psr\Http\Message\ResponseInterface|\ZohoInvoice\Invoice\ZohoResponse\ZohoCollectionInvoiceResponse|ZohoResponse
     * @throws InvoiceException
     */
    public function getByStatusAndCustomer(ZohoId $zohoId, $status)
    {
        try {
            $uri = sprintf(
                'invoices?authtoken=%s&status=%s&customer_id=%s',
                $this->config['authtoken'],
                $status,
                $zohoId->toString()
            );
            $response = $this->client->request('GET', $uri);

            if (200 == $response->getStatusCode()) {
                $body = $response->getBody();
                return (new ZohoInvoiceTranslator())->convertToCollectionResponse($body->getContents());
            }

            $this->log($response->getReasonPhrase());
            $this->log($response->getStatusCode());

            throw new InvoiceException('Zoho get invoice no response');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                if (404 == $e->getResponse()->getStatusCode()) {
                    $response = new ZohoResponse();
                    $response->exist = false;

                    return $response;
                } else {
                    $this->log(Psr7\str($e->getResponse()));
                }
            }

            $this->log(Psr7\str($e->getRequest()));

            throw new InvoiceException('Zoho exception');
        }
    }
}
