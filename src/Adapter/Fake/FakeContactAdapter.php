<?php

namespace ZohoInvoice\Adapter\Fake;

use ZohoInvoice\Adapter\ContactAdapterInterface;
use ZohoInvoice\Invoice\ZohoTranslator\ZohoContactTranslator;
use ZohoInvoice\ZohoContact\ZohoContact;
use ZohoInvoice\ZohoContact\ZohoId;

class FakeContactAdapter implements ContactAdapterInterface
{
    public function getContact(ZohoId $zohoId)
    {
        $body = ['contact' => ['contact_id' => $zohoId->toString()]];

        return (new ZohoContactTranslator())->convertToSingleResponse(json_encode($body));
    }

    public function createContact(ZohoContact $zohoContact)
    {
        $body = ['contact' => ['contact_id' => rand(2000, 5000)]];

        return (new ZohoContactTranslator())->convertToSingleResponse(json_encode($body));
    }

    public function updateContact(ZohoContact $zohoContact)
    {
        return $this->getContact($zohoContact->zohoId());
    }

    public function createLightContact(ZohoContact $zohoContact)
    {
        return $this->createContact($zohoContact);
    }

    public function getContacts()
    {
        // TODO: Implement getContacts() method.
    }
}