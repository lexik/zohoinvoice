<?php

namespace ZohoInvoice\Invoice\ZohoInvoice;

use Webmozart\Assert\Assert;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;

/**
 * Class ZohoInvoice
 * @package ZohoInvoice\ZohoInvoice
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class ZohoInvoice
{
    const STATUS_UNPAID = 'unpaid';

    /**
     * @var ZohoInvoiceId
     */
    private $zohoInvoiceId;

    /**
     * @var ZohoId
     */
    private $zohoContactId;

    /**
     * @var array|ZohoInvoiceItem[]
     */
    private $items;

    private function __construct(ZohoInvoiceId $zohoInvoiceId, array $items)
    {
        $this->zohoInvoiceId = $zohoInvoiceId;
        $this->items = $items;
    }

    public static function create(ZohoInvoiceId $zohoInvoiceId, array $items)
    {
        Assert::notEmpty($items);

        return new self($zohoInvoiceId, $items);
    }

    public function zohoInvoiceId()
    {
        return $this->zohoInvoiceId;
    }

    /**
     * @return ZohoId
     */
    public function zohoContactId()
    {
        return $this->zohoContactId;
    }

    /**
     * @return array|ZohoInvoiceItem[]
     */
    public function items()
    {
        return $this->items;
    }

}