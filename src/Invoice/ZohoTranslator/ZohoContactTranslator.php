<?php

namespace ZohoInvoice\Invoice\ZohoTranslator;

use ZohoInvoice\Invoice\ZohoResponse\ZohoCollectionContactResponse;
use ZohoInvoice\Invoice\ZohoResponse\ZohoSingleContactResponse;

/**
 * Class ZohoContactTranslator
 * @package ZohoInvoice\Invoice\ZohoTranslator
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class ZohoContactTranslator
{
    public function convertToSingleResponse($contents)
    {
        $arrayDecode = json_decode($contents, true);

        return ZohoSingleContactResponse::createSingle($arrayDecode['contact']);
    }

    public function convertToCollectionResponse($contents)
    {
        $arrayDecode = json_decode($contents, true);

        return ZohoCollectionContactResponse::createMany($arrayDecode['contacts']);
    }
}
