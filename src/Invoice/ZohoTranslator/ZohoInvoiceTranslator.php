<?php

namespace ZohoInvoice\Invoice\ZohoTranslator;

use ZohoInvoice\Invoice\ZohoResponse\ZohoCollectionInvoiceResponse;
use ZohoInvoice\Invoice\ZohoResponse\ZohoInvoiceResponse;

class ZohoInvoiceTranslator
{
    public static function convertToResponse($contents)
    {
        $arrayDecode = json_decode($contents, true);

        $invoiceId = $arrayDecode['invoice']['invoice_id'];

        return ZohoInvoiceResponse::create($invoiceId);
    }

    public static function convertToCollectionResponse($contents)
    {
        $arrayDecode = json_decode($contents, true);

        return ZohoCollectionInvoiceResponse::createMany($arrayDecode['invoices']);
    }
}
