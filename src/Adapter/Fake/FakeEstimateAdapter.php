<?php

namespace ZohoInvoice\Adapter\Fake;

use ZohoInvoice\Adapter\InvoiceEstimateAdapterInterface;

class FakeEstimateAdapter implements InvoiceEstimateAdapterInterface
{
    public function createQuote()
    {
        // TODO: Implement createQuote() method.
    }

    public function updateQuote()
    {
        // TODO: Implement updateQuote() method.
    }

    public function sendQuoteToEmail()
    {
        // TODO: Implement sendQuoteToEmail() method.
    }

    public function downloadQuote()
    {
        // TODO: Implement downloadQuote() method.
    }

    public function getQuote()
    {
        // TODO: Implement getQuote() method.
    }
}