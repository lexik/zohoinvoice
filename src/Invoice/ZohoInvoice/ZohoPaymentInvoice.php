<?php

namespace ZohoInvoice\Invoice\ZohoInvoice;

class ZohoPaymentInvoice
{
    /**
     * @var ZohoPayment
     */
    private $zohoPayment;

    /**
     * @var ZohoInvoiceId
     */
    private $zohoInvoiceId;

    private $amount;

    public function __construct($zohoPayment, $zohoInvoiceId, $amount)
    {
        $this->zohoPayment = $zohoPayment;
        $this->zohoInvoiceId = $zohoInvoiceId;
        $this->amount = $amount;
    }

    public static function create(ZohoPayment $zohoPayment, ZohoInvoiceId $zohoInvoiceId, $amount)
    {
        return new self($zohoPayment, $zohoInvoiceId, $amount);
    }

    public function zohoPayment()
    {
        return $this->zohoPayment;
    }

    public function zohoInvoiceId()
    {
        return $this->zohoInvoiceId;
    }

    public function amount()
    {
        return $this->amount;
    }
}