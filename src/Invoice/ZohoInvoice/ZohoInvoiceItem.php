<?php

namespace ZohoInvoice\Invoice\ZohoInvoice;

use Webmozart\Assert\Assert;

class ZohoInvoiceItem
{
    private $itemId;

    private $name;

    private $rate;

    private $quantity;

    private function __construct($itemId, $name, $rate, $quantity)
    {
        $this->itemId = $itemId;
        $this->name = $name;
        $this->rate = $rate;
        $this->quantity = $quantity;
    }

    public static function createItem($itemId, $name, $rate, $quantity)
    {
        Assert::notEmpty($itemId);
        Assert::notEmpty($name);
        Assert::greaterThan($quantity, 1);

        return new self($itemId, $name, $rate, $quantity);
    }

    /**
     * @return mixed
     */
    public function itemId()
    {
        return $this->itemId;
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function rate()
    {
        return $this->rate;
    }

    /**
     * @return mixed
     */
    public function quantity()
    {
        return $this->quantity;
    }
}