<?php

namespace ZohoInvoice\Invoice\ZohoResponse;

class ZohoQuoteResponse extends ZohoResponse
{
    public $estimateId;

    public $totalAmount;

    public static function create($estimateId, $totalAmount = 0)
    {
        $response = new self();
        $response->estimateId = $estimateId;
        $response->totalAmount = $totalAmount;

        return $response;
    }
}
