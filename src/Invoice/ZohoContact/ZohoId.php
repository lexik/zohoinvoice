<?php

namespace ZohoInvoice\Invoice\ZohoContact;

/**
 * Class ZohoId
 * @package ZohoInvoice\Invoice
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class ZohoId
{
    /**
     * @var string
     */
    private $zohoId;

    /**
     * ZohoId constructor.
     * @param $zohoId
     */
    private function __construct($zohoId)
    {
        $this->zohoId = $zohoId;
    }

    /**
     * @param $zohoId
     * @return ZohoId
     */
    public static function createFromString($zohoId)
    {
        return new self($zohoId);
    }

    /**
     * @return string
     */
    public function toString()
    {
        return (string) $this->zohoId;
    }
}