<?php

namespace ZohoInvoice\Adapter;

use ZohoInvoice\Invoice\InvoiceException;
use ZohoInvoice\Invoice\ZohoContact\ZohoContact;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoice;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoInvoiceId;
use ZohoInvoice\Invoice\ZohoInvoice\ZohoPayment;
use ZohoInvoice\Invoice\ZohoResponse\ZohoInvoiceResponse;
use ZohoInvoice\Invoice\ZohoResponse\ZohoResponse;

/**
 * Interface InvoiceInvoiceAdapter.
 */
interface InvoiceInvoiceAdapterInterface extends InvoiceAdapterInterface
{
    /**
     * @param ZohoInvoiceId $zohoInvoiceId
     *
     * @return ZohoInvoiceResponse
     */
    public function getInvoice(ZohoInvoiceId $zohoInvoiceId);

    /**
     * @param ZohoInvoice $zohoInvoice
     *
     * @return ZohoInvoiceResponse
     */
    public function createInvoice(ZohoInvoice $zohoInvoice);

    /**
     * @param ZohoPayment $zohoPayment
     * @return mixed
     */
    public function createPaymentInvoice(ZohoPayment $zohoPayment);

    /**
     * @param ZohoPayment $zohoPayment
     * @param ZohoContact $zohoContact
     * @return mixed
     */
    public function emailAnInvoice(ZohoPayment $zohoPayment, ZohoContact $zohoContact);

    /**
     * @param string
     * @return mixed
     */
    public function downloadInvoice($invoicesIds);

    /**
     * @param ZohoId $zohoId
     * @param $status
     * @return mixed|\Psr\Http\Message\ResponseInterface|\ZohoInvoice\Invoice\ZohoResponse\ZohoCollectionInvoiceResponse|ZohoResponse
     * @throws InvoiceException
     */
    public function getByStatusAndCustomer(ZohoId $zohoId, $status);
}
