<?php

namespace ZohoInvoice\Invoice\ZohoTranslator;

use ZohoInvoice\Invoice\ZohoResponse\ZohoQuoteResponse;

class ZohoQuoteTranslator
{
    public function convertToResponse($contents)
    {
        $arrayDecode = json_decode($contents, true);

        $estimateId = $arrayDecode['estimate']['estimate_id'];
        $totalAmount = $arrayDecode['estimate']['total'] * 100;

        return ZohoQuoteResponse::create($estimateId, $totalAmount);
    }
}
