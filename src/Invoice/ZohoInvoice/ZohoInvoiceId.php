<?php

namespace ZohoInvoice\Invoice\ZohoInvoice;

/**
 * Class ZohoInvoiceId
 * @package ZohoInvoice\ZohoInvoice
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class ZohoInvoiceId
{
    /**
     * @var string
     */
    private $zohoInvoiceId;

    /**
     * ZohoInvoiceId constructor.
     * @param $zohoInvoiceId
     */
    private function __construct($zohoInvoiceId)
    {
        $this->zohoInvoiceId = $zohoInvoiceId;
    }

    /**
     * @param $zohoInvoiceId
     * @return ZohoInvoiceId
     */
    public static function createFromString($zohoInvoiceId)
    {
        return new self($zohoInvoiceId);
    }

    /**
     * @return string
     */
    public function toString()
    {
        return (string) $this->zohoInvoiceId;
    }
}