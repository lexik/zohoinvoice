<?php

namespace ZohoInvoice\Adapter\Http;

use GuzzleHttp\Psr7;
use ZohoInvoice\Adapter\InvoiceEstimateAdapterInterface;

/**
 * Class HttpZohoEstimateAdapter.
 *
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class HttpZohoEstimateAdapter extends AbstractHttpZohoAdapter implements InvoiceEstimateAdapterInterface
{

    public function createQuote()
    {
        // TODO: Implement createQuote() method.
    }

    public function updateQuote()
    {
        // TODO: Implement updateQuote() method.
    }

    public function sendQuoteToEmail()
    {
        // TODO: Implement sendQuoteToEmail() method.
    }

    public function downloadQuote()
    {
        // TODO: Implement downloadQuote() method.
    }

    public function getQuote()
    {
        // TODO: Implement getQuote() method.
    }
}
