<?php

namespace ZohoInvoice\Adapter\Fake;

use ZohoInvoice\Adapter\InvoiceInvoiceAdapterInterface;
use ZohoInvoice\Invoice\InvoiceException;
use ZohoInvoice\Invoice\ZohoResponse\ZohoInvoiceResponse;
use ZohoInvoice\Invoice\ZohoResponse\ZohoResponse;
use ZohoInvoice\Invoice\ZohoTranslator\ZohoInvoiceTranslator;
use ZohoInvoice\ZohoContact\ZohoContact;
use ZohoInvoice\ZohoInvoice\ZohoInvoice;
use ZohoInvoice\ZohoInvoice\ZohoInvoiceId;
use ZohoInvoice\ZohoInvoice\ZohoPayment;

class FakeInvoiceAdapter implements InvoiceInvoiceAdapterInterface
{
    public function getInvoice(ZohoInvoiceId $zohoInvoiceId)
    {
    }

    /**
     * @param ZohoInvoice $zohoInvoice
     *
     * @return ZohoInvoiceResponse
     */
    public function createInvoice(ZohoInvoice $zohoInvoice)
    {
        $body = ['invoice' => ['invoice_id' => rand(2000, 5000)]];
        $response = ZohoInvoiceTranslator::convertToResponse(json_encode($body));
        $response->exist = true;

        return $response;
    }

    /**
     * @param ZohoPayment $zohoPayment
     *
     * @throws InvoiceException
     *
     * @return mixed|ZohoInvoiceResponse|\Psr\Http\Message\ResponseInterface
     */
    public function createPaymentInvoice(ZohoPayment $zohoPayment)
    {
        $response = ZohoInvoiceResponse::create($zohoPayment->singleInvoice()->zohoInvoiceId()->toString());

        return $response;
    }

    /**
     * @param ZohoPayment $zohoPayment
     * @param ZohoContact $zohoContact
     * @return ZohoResponse
     */
    public function emailAnInvoice(ZohoPayment $zohoPayment, ZohoContact $zohoContact)
    {
        return new ZohoResponse();
    }

    /**
     * @param ZohoInvoiceId $zohoInvoiceId
     *
     * @return mixed
     */
    public function downloadInvoice(ZohoInvoiceId $zohoInvoiceId)
    {
        return file_get_contents(__DIR__.'/invoice.pdf');
    }
}