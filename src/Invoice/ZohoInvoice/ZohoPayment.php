<?php

namespace ZohoInvoice\Invoice\ZohoInvoice;
use ZohoInvoice\Invoice\ZohoContact\ZohoId;

/**
 * Class ZohoPayment
 * @package ZohoInvoice\ZohoInvoice
 * @author Gauthier Gilles <g.gauthier@lexik.fr>
 */
class ZohoPayment
{
    const PAYMENT_MODE_CREDIT_CARD = 'creditcard';

    /**
     * @var ZohoId;
     */
    private $zohoId;

    /**
     * @var string
     */
    private $paymentMode;

    /**
     * @var array|ZohoPaymentInvoice[]
     */
    private $paymentInvoices;

    /**
     * @var ZohoPaymentInvoice
     */
    private $singleInvoice;

    private $amount;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $description;

    public static function createMultipleInvoices(
        ZohoId $zohoId, array $paymentInvoices, $date, $description, $amount,
        $paymentMode = self::PAYMENT_MODE_CREDIT_CARD)
    {
        $self = new self();
        $self->zohoId = $zohoId;
        $self->paymentMode = $paymentMode;
        $self->paymentInvoices = $paymentInvoices;
        $self->amount = $amount;
        $self->date = $date;
        $self->description = $description;

        return $self;
    }

    public static function createSingleInvoice(
        ZohoId $zohoId, $paymentInvoice, $date, $description, $amount,
        $paymentMode = self::PAYMENT_MODE_CREDIT_CARD)
    {
        $self = new self();
        $self->zohoId = $zohoId;
        $self->paymentMode = $paymentMode;
        $self->amount = $amount;
        $self->singleInvoice = $paymentInvoice;
        $self->date = $date;
        $self->description = $description;

        return $self;
    }

    /**
     * @return string
     */
    public function paymentMode()
    {
        return $this->paymentMode;
    }

    /**
     * @return ZohoId
     */
    public function zohoId()
    {
        return $this->zohoId;
    }

    /**
     * @return array|ZohoPaymentInvoice[]
     */
    public function paymentInvoices()
    {
        return $this->paymentInvoices;
    }

    /**
     * @return ZohoPaymentInvoice
     */
    public function singleInvoice()
    {
        return $this->singleInvoice;
    }

    public function amount()
    {
        return $this->amount;
    }

    /**
     * @return \DateTime
     */
    public function date()
    {
        return $this->date;
    }

    public function description()
    {
        return $this->description;
    }
}